import React from 'react';
import './ModalTwo.scss';

const ModalTwo = ({ show, setShow }) => {
    return (
        <div className={show ? 'myModal active' : 'myModal'} onClick={() => setShow(false)}>
            <div className='myModal__content' onClick={e => e.stopPropagation()}>
                <div className='myModal__content_header'>
                    <p className='myModal__content_text'>Do you want to leave?</p>
                    <img className='myModal__content_img' src="/img/cross.png" alt="icon" />
                </div>
                <div className='myModal__content_description'>
                    <p className='myModal__content_description_text'>Stay here</p>
                    <div className='myModal__content_description_buttons'>
                        <button className='myModal__content_description_agree'>Ok</button>
                        <button className='myModal__content_description_disagree'>Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default ModalTwo;