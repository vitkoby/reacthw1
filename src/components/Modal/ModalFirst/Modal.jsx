import React from 'react';
import './Modal.scss';

const Modal = ({ active, setActive }) => {
    return (
        <div className={active ? 'modal active' : 'modal'} onClick={() => setActive(false)}>
            <div className='modal__content' onClick={e => e.stopPropagation()}>
                <div className='modal__content_header'>
                    <p className='modal__content_text'>Do you want to delete this file?</p>
                    <img className='modal__content_img' src="/img/cross.png" alt="icon" />
                </div>
                <div className='modal__content_description'>
                    <p className='modal__content_description_text'>Once you delete this file, it won’t be possible to undo this action. <br /> Are you sure you want to delete it?</p>
                    <div className='modal__content_description_buttons'>
                        <button className='modal__content_description_agree'>Ok</button>
                        <button className='modal__content_description_disagree'>Cancel</button>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default Modal;