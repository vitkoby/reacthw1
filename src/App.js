import { useState } from 'react';
import './App.scss';
import Modal from './components/Modal/ModalFirst/Modal';
import ModalTwo from './components/Modal/ModalSecond/ModalTwo';

function App() {
    const [modalActive, setModalActive] = useState(true);
    const [showActive, setShowActive] = useState(true);
    return (
        <div className="app">
            <div className='container'>
                <button className='buttonModal' onClick={() => setModalActive(true)}>Open first modal</button>
                <button className='buttonModal' onClick={() => setShowActive(true)}>Open second modal</button>
            </div>
            {<Modal active={modalActive} setActive={setModalActive} />}
            {<ModalTwo show={showActive} setShow={setShowActive} />}
        </div>
    )
}

export default App;